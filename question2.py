import numpy as np


class Temptracker:
    def __init__(self, temp_list=None):
        if temp_list is not None:
            self.temp_list = temp_list

    """
    insert a temperature to the recorded temperature list
    temperature must be integer
    """
    def insert(self, temperature):
        if type(temperature) == int:
            self.temp_list.append(temperature)
            print('The new temperature has been recorded.')
        else:
            print('The temperature should be in the range of 0-110')

    """
    get the highest temperature from the recorded temperature list
    """
    def get_max(self):
        max_temp = max(self.temp_list)
        print("The highest temperature recorded is " + str(max_temp))
        return max_temp

    """
    get the lowest temperature from the recorded temperature list
    """
    def get_min(self):
        min_temp = min(self.temp_list)
        print("The highest temperature recorded is " + str(min_temp))
        return min_temp

    """
    get the mean of recorded temperature list
    """
    def get_mean(self):
        mean_temp = float('%.2f' % np.mean(self.temp_list))
        print("The mean of all temperatures is " + str(mean_temp))
        return mean_temp


if __name__ == '__main__':
    temp_list = [11, 22, 33]
    tempTracker = Temptracker(temp_list)
    tempTracker.get_mean()