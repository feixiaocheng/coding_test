"""
Input an array of arbitrarily nested arrays of integers
"""


def flatten_array(array):
    for item in array:
        try:
            yield from flatten_array(item)
        except TypeError:
            yield item


if __name__ == '__main__':
    sample_array = [[[1, 2, 3], [4, 5]], 6]
    flattened_array = list(flatten_array(sample_array))
    print(flattened_array)